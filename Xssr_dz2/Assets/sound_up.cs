﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound_up : MonoBehaviour
{

    [FMODUnity.EventRef] public string eventjump;
    [FMODUnity.EventRef] public string eventlandjump;
    public LayerMask sloy;
    public float tippoverhonsti;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void jump()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(eventjump, gameObject);
    }

    void landjump()
    {
        Proverkapoverhnosti();
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventlandjump);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        eventInstance.setParameterByName("Prizemlenie type", tippoverhonsti);
        eventInstance.start();
        eventInstance.release();

        Debug.Log("Упал");



    }
    void Proverkapoverhnosti()
    {

        RaycastHit luchh;
        if (Physics.Raycast(transform.position, Vector3.down, out luchh, 03f, sloy))
        {
          
            if (luchh.collider.CompareTag("vMildSnow")) tippoverhonsti = 0;
            else if (luchh.collider.CompareTag("vWood")) tippoverhonsti = 1;
            else if (luchh.collider.CompareTag("vWater")) tippoverhonsti = 2;
            else tippoverhonsti = 0f;
        }

    }

}
