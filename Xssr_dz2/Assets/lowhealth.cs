﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using Invector.vCharacterController;
using Invector.vCharacterController.AI;

public class lowhealth : MonoBehaviour

{
    [FMODUnity.EventRef] public string snapevent;
    FMOD.Studio.EventInstance snapInstance;
    [FMODUnity.EventRef] public string heartbeat;
    FMOD.Studio.EventInstance heartbeatInstance;
    public vThirdPersonController control;
    public bool SnaphotIsPlaying = false;
    public FMOD.Studio.PLAYBACK_STATE playback;
    public vControlAIMelee aicontrol;




    // Start is called before the first frame update
    void Start()
    {
        snapInstance = FMODUnity.RuntimeManager.CreateInstance(snapevent);
        heartbeatInstance = FMODUnity.RuntimeManager.CreateInstance(heartbeat);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(aicontrol.currentHealth);
        if (control.isDead == true)
        {
            snapInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            heartbeatInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            
        }
    }

    public void HealthSnapshotStart ()
    {
        snapInstance.getPlaybackState(out playback);
        if (playback != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            snapInstance.start();
            heartbeatInstance.start();
            Debug.Log(playback);
        }

    }

    public void HealthSnapshotStop()
    {
        if (control.currentHealth > 30)
        {
            snapInstance.getPlaybackState(out playback);
            if (playback == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                snapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                heartbeatInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
    }

}

