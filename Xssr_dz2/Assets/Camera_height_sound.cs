﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_height_sound : MonoBehaviour
{
    public GameObject camera;
    [FMODUnity.EventRef] public string windevent;
    FMOD.Studio.EventInstance windinstance;
    public DayNightController dnc;
    //public GameObject daynight;

    // Start is called before the first frame update
    void Start()
    {
        windinstance = FMODUnity.RuntimeManager.CreateInstance(windevent);
        windinstance.start();
        //dnc = daynight.GetComponent<DayNightController>();
    }

    // Update is called once per frame
    void Update()
    {
        windinstance.setParameterByName("Camera_height", camera.transform.position.y);
        //windinstance.setParameterByName("DayNight", dnc.currentTime);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("DayNight",dnc.currentTime);//передает глобальное значение параметра 
        Debug.Log(dnc.currentTime);
    }
}
