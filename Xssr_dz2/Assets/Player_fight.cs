﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using Invector.vCharacterController;

public class Player_fight : MonoBehaviour
{
    [FMODUnity.EventRef] public string Hitevent;
    [FMODUnity.EventRef] public string udarnogoj;
    public vThirdPersonInput PlayerInput;


    // Start is called before the first frame update
    void Start()
    {
        PlayerInput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayerHit()
    {
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Hitevent);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        eventInstance.start();
        eventInstance.release();
    }
    public void udarnoga()
    {
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(udarnogoj);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        eventInstance.start();
        eventInstance.release();
    }
}
