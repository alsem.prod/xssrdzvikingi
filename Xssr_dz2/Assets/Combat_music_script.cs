﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;
using FMODUnity;
using Invector.vCharacterController;

public class Combat_music_script : MonoBehaviour
{

    public vControlAIMelee enemycontroller;
    public vThirdPersonController playercontroller;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enemycontroller.isInCombat == true)
        {
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Intensity", 90);
        }
        else FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Intensity", 25);

        if (playercontroller.isDead == true)
        {
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Intensity", 97);
        }

         
    }

   
    
}
