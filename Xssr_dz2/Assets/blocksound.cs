﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class blocksound : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string blocksoundevent;
    [FMODUnity.EventRef]
    public string defsound;
    [FMODUnity.EventRef]
    public string meeledefsound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void block()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(blocksoundevent, gameObject);
    }

    void def()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(defsound, gameObject);
    }

    void meeledef()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(meeledefsound, gameObject);
    }

}
