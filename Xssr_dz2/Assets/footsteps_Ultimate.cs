﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class footsteps_Ultimate : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string footstepsevent;
    public vThirdPersonInput tpinput;
    public LayerMask sloy;
    public float tippoverhnosti;


    // Start is called before the first frame update
    void Start()
    {
        tpinput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
 
    }

void footsteps ()
    {
        if (tpinput.cc.inputMagnitude > 0.1)
        {
            
            proverkapoverhnosti();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            if (tpinput.cc.inputMagnitude>0.5)
            {

                eventInstance.setParameterByName("isrunning", 1);
                Debug.Log("Run"); ;
            }
            else eventInstance.setParameterByName("isrunning", 0);
            eventInstance.setParameterByName("surface type", tippoverhnosti);
            eventInstance.start();
            eventInstance.release();

        }
        

   
    }
    void proverkapoverhnosti()
    {
        RaycastHit luch;
        if (Physics.Raycast(transform.position, Vector3.down, out luch, 03f, sloy))
        {
            Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("vMildSnow")) tippoverhnosti = 0;
            else if (luch.collider.CompareTag("vWood")) tippoverhnosti = 1;
            else if (luch.collider.CompareTag("vWater")) tippoverhnosti = 2;
            else if (luch.collider.CompareTag("vPomeshenii")) tippoverhnosti = 3;
            else tippoverhnosti = 0;

        }
    }
}

