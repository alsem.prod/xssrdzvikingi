﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class Sound_footsteps : MonoBehaviour
{
    [FMODUnity.EventRef] public string footstepsevent;
    public vThirdPersonInput vikinginput;
    [FMODUnity.EventRef] public string jumphujakevent;

    // Start is called before the first frame update
    void Start()
    {
        vikinginput = GetComponent<vThirdPersonInput>(); 
    }

    // Update is called once per frame
    void Update()
    {

    }


   void footsteps()
    {
        if (vikinginput.cc.inputMagnitude > 0.1)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(footstepsevent, gameObject);
        }
  
    }
    void jump()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(jumphujakevent, gameObject);
    }












}
